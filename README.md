# **Flow demo**  

 Flow demo for matching website using numerology and astrology compatibility. 

### Used software: ###

**Languages**

* C#, JavaScript

**Database**

* MS SQL Server 2014

**ORM**

* Entity Framework 6 (Code First)

**Internet**

* ASP.NET MVC 4,  JavaScript, jQuery, Templates, Ajax, CSS

**Responsive web design**

* front-end web framework - [Zurb Foundation](http://foundation.zurb.com/)


_____________________________________________

# **Screenshots** 


## ** Compatibility calculator**

_____________________________________________

![res.png](https://bitbucket.org/repo/9pK8eox/images/460298488-res.png)



_____________________________________________

## **User profile/ Personal info**

_____________________________________________


![t54.png](https://bitbucket.org/repo/9pK8eox/images/1315199278-t54.png)


_____________________________________________


## **User profile/ Criteria for search **


*Used point, range and set filters*

_____________________________________________

![M24.jpg](https://bitbucket.org/repo/9pK8eox/images/3207290714-M24.jpg)


_____________________________________________



##  **User search form **

____________________________________________


![77657.png](https://bitbucket.org/repo/9pK8eox/images/649692503-77657.png)
_____________________________________________

##  **User search form (mobile)**

____________________________________________

![M21.jpg](https://bitbucket.org/repo/9pK8eox/images/3691968947-M21.jpg)

____________________________________________






##  **Search result**

_____________________________________________

![M5.jpg](https://bitbucket.org/repo/9pK8eox/images/1684047259-M5.jpg)

_____________________________________________


## **Search result/ Profile**

_____________________________________________

![M9.jpg](https://bitbucket.org/repo/9pK8eox/images/2470251244-M9.jpg)

_____________________________________________

##  **Search result/ Profile/ Personal info**

_____________________________________________

![M10.jpg](https://bitbucket.org/repo/9pK8eox/images/3246343572-M10.jpg)

_____________________________________________

## **Search result/  Profile/ Zodiac compatibility**

_____________________________________________

![M2.jpg](https://bitbucket.org/repo/9pK8eox/images/2283096134-M2.jpg)

_____________________________________________

## **Search result/ Profile/ Numerology compatibility**

_____________________________________________

![M3.jpg](https://bitbucket.org/repo/9pK8eox/images/3210739136-M3.jpg)

_____________________________________________