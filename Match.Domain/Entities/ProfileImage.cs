﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Match.Domain.Abstract;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.IO;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure;

namespace Match.Domain.Entities
{
    public class ProfileImage : EntityBase<int>, IAttribute
    {
        public virtual ICollection<Profile> Profiles { get; set; }

        public string Name { get; set; }
        public byte[] ImageData { get; set; }
        public string ImageMimeType { get; set; }
        public int ImageOrderId { get; set; }
       





        public void SaveImageFromHttp(HttpPostedFileBase image)
        {

            ImageMimeType = image.ContentType;
            ImageData = new byte[image.ContentLength];
            image.InputStream.Read(ImageData, 0, image.ContentLength);
        }



        public void SaveImageFromDisc(string filePath)
        {
            //open file from the disk (file path is the path to the file to be opened)
            using (FileStream fileStream = File.OpenRead(filePath))
            {
                ImageData = new byte[fileStream.Length];
                fileStream.Read(ImageData, 0, (int)fileStream.Length);
                ImageMimeType = "image/jpeg";
            }
        }
    }
}
