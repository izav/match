﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Match.Domain.Abstract;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.IO;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure;



namespace Match.Domain.Entities
{
   
    public class Criteria : EntityBase<int>, IAggregateRoot
    {
        [Key, ForeignKey("Id")]
        public Profile Profile { get; set; }


        
        // Simple Filter
        //----------------------------------------
        [DisplayName("Gender")]
        [Required]
        public int LookingForId { get; set; }
        [NotMapped]
        public Gender LookingForValue
        {
            get { return (Gender)LookingForId; }
            set { LookingForId = (int)value; }
        }



        //-------- Numerology Filter
        //-----------------------
        public int PersonalityNumberId { get; set; }
        public PersonalityNumber PersonalityNumber
        {
            get
            {
                return PersonalityNumber.GetPersonalityNumber(PersonalityNumberId);
            }

        }


        //-------- Zodiac Filter
        //-----------------------
        public int ZodiacSignId { get; set; }

        public string ZodiacSignName
        {
            get
            {
                return ZodiacSign.GetZodiacSignName(ZodiacSignId);
            }

        }

        public ZodiacSign ZodiacSign
        {
            get
            {
                return ZodiacSign.GetZodiacSign(ZodiacSignId);
            }

        }

        //--------  Filter  OR    ----------------
        //--------------------------------------

        [DisplayName("Status")]
        //public virtual ICollection<Status> SearchStatuses { get; set; }
      
        [NotMapped]
        public int[] SearchStatusIdMask { get; set; }
        [NotMapped]
        public int[] SearchStatusIds { get; set; }
     
        //---------------------------------------
        
        [DisplayName("Education")]
        public long SearchEducationGradeIdMask { get; set; }
        [NotMapped]
        public int[] SearchEducationGradeIds { get; set; }

        //-----------------------------------------

        [DisplayName("Religion")]
        public long SearchReligionIdMask { get; set; }

        [NotMapped]
        public int[] SearchReligionIds { get; set; }

        //--------------------------------------------------------------

        [DisplayName("Smoke")]
        public long SearchSmokeIdMask { get; set; }
        [NotMapped]
        public int[] SearchSmokeIds { get; set; }

        //-------------------------------------

        [DisplayName("Disability")]
        public long SearchDisabilityIdMask { get; set; }
        [NotMapped]
        public int[] SearchDisabilityIds { get; set; }

        //------------------------------------------
        
        [DisplayName("Race")]
        public long SearchRaceIdMask { get; set; }
        [NotMapped]
        public int[] SearchRaceIds { get; set; }

        //------------------- 

        [DisplayName("Occupation")]
        public long SearchOccupationIdMask { get; set; }
        [NotMapped]
        public int[] SearchOccupationIds { get; set; }

        //-------------- Filter From-To-----------
        //------------------------------------------
       
        [DisplayName("Income")]
        [Range(1, int.MaxValue, ErrorMessage = "Must be selected")]
        public int? SearchIncomeIdFrom { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Must be selected")]
        public int? SearchIncomeIdTo { get; set; }

        [ForeignKey("SearchIncomeIdFrom")]
        public Income SearchIncomeFrom { get; set; }
        [ForeignKey("SearchIncomeIdTo")]
        public Income SearchIncomeTo { get; set; }

        //------------------------------

       [Range(1, int.MaxValue, ErrorMessage = "Must be selected")]
        public int? SearchHeightIdFrom { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Must be selected")]
        public int? SearchHeightIdTo { get; set; }

        [ForeignKey("SearchHeightIdFrom")]
        public Height SearchHeightFrom { get; set; }
        [ForeignKey("SearchHeightIdTo")]
        public Height SearchHeightTo { get; set; }

        //--------------------------------------
        
        [Range(1, int.MaxValue, ErrorMessage = "Must be selected")]
        public int? SearchWeightIdTo { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Must be selected")]
        public int? SearchWeightIdFrom { get; set; }

        [ForeignKey("SearchWeightIdFrom")]
        public Weight SearchWeightFrom { get; set; }
        [ForeignKey("SearchWeightIdTo")]
        public Weight SearchWeightTo { get; set; }

        //-------------------------------------
      
        [Range(1, int.MaxValue, ErrorMessage = "Must be selected")]
        public int? SearchAgeIdFrom { get; set; }
        [ForeignKey("SearchAgeIdTo")]
        public Age SearchAgeTo { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Must be selected")]
        public int? SearchAgeIdTo { get; set; }
        [ForeignKey("SearchAgeIdFrom")]
        public Age SearchAgeFrom { get; set; }


        //---------------------------------

        [Range(1, int.MaxValue, ErrorMessage = "Must be selected")]
        public int? SearchChildrenIdFrom { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "Must be selected")]
        public int? SearchChildrenIdTo { get; set; }

        [ForeignKey("SearchChildrenIdFrom")]
        public Children SearchChildrenFrom { get; set; }
        [ForeignKey("SearchChildrenIdTo")]
        public Children SearchChildrenTo { get; set; }

    }
}
