﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Match.Domain.Abstract;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure;

namespace Match.Domain.Entities
{
    public class Smoke : EntityBase<int>, IAttribute
    {
        public string Name { get; set; }
       
        [InverseProperty("SmokeId")]
        public virtual IEnumerable<Profile> Profiles { get; set; }

       // [InverseProperty("SearchSmokes")]
      //  public virtual ICollection<Profile> SearchProfiles { get; set; }


        public static string[] InitValue = { "Non-Smoker", "Occasionally", "Regularly", "Trying to quit" };
    }
}    
