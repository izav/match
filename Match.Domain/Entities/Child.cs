﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Match.Domain.Abstract;

namespace Match.Domain.Entities
{
    public class Children : EntityBase<int>, IAttribute
    {
        public string Name { get; set; }
        
        public static string[] InitValue = { "None", "1", "2", "3", "4", "5", "6", "7", "8", "9 and more" };
    }
}
