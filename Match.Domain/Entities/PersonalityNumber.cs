﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Match.Domain.Abstract;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.IO;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure;



namespace Match.Domain.Entities
{
    //-----------------------------------------------
    public class NumeralogyCompatibilityLevel : EntityBase<int>, IAttribute
    {
        public  string Name { get; set; }
        public string Description { get; set; }
    }
    //---------------------------------------------------
    public  class PersonalityNumber : EntityBase<int>, IAttribute
    {
        // http://astrology-numerology.com/num-relationship.html

        public string Name { get; set; }

        [DisplayName("Natural fit")]
        public int HighMask { get; set; }

        [DisplayName("Good compatibility")]
        public int GoodMask { get; set; }

        [DisplayName("Fairly compatible")]
        public int FairlyMask { get; set; }

        [DisplayName("Low compatibility")]
        public int LowMask { get; set; }

        //---------------------------------- 
        public static NumeralogyCompatibilityLevel[] NumeralogyCompatibilityLevels = { 
            new NumeralogyCompatibilityLevel() { Id = 1, Name = "High compatibility",
             Description = "Numbers that produce easy compatibility or seem to be a natural fit."},
            
         new NumeralogyCompatibilityLevel() { Id = 2, Name = "Good compatibility",
             Description = "Numbers that are usually compatible and find it easy to get along."},
           
         new NumeralogyCompatibilityLevel() { Id = 3, Name = "Fairly compatible",
             Description ="Numbers that can go either way - or may be neutral" },

         new NumeralogyCompatibilityLevel() { Id = 4, Name = "Low compatibility" ,
             Description ="Numbers that are often a bit of a challenge and require much compromise."},
     };

//----------------------------------------------------------------------------
        public static NumeralogyCompatibilityLevel GetNumeralogyCompatibility(int p1, int p2)
        {
            
            PersonalityNumber z2 = GetPersonalityNumber(p2);
            int id = 0;
            int maskP1 = (int)(Math.Pow(2, p1));
            if ((z2.HighMask & maskP1) > 0) id = 1;
            if ((z2.GoodMask & maskP1) > 0) id = 2;
            if ((z2.FairlyMask & maskP1) > 0) id = 3;
            if ((z2.LowMask & maskP1) > 0) id = 4;
            NumeralogyCompatibilityLevel c = (id == 0) ? null : NumeralogyCompatibilityLevels[id - 1];
            return c;

        }



        
   //-----------------------------------------------------------------------------       
        public static PersonalityNumber GetPersonalityNumber(int Id)
        {
         
         if (Id==1)
                        {   return new PersonalityNumber() {
                            Id = 1,
                            HighMask = (int)(Math.Pow(2, 1) +Math.Pow(2, 5)  + Math.Pow(2, 7)),
                            GoodMask = (int)(Math.Pow(2, 3)  + Math.Pow(2, 9)),
                            FairlyMask = (int)(Math.Pow(2, 8)),
                            LowMask = (int)(Math.Pow(2, 2)  +Math.Pow(2, 4)+ Math.Pow(2, 6))}; 
                        };
         if (Id==2)
                        {   return new PersonalityNumber() {
                            Id = 2,
                            HighMask = (int)(Math.Pow(2, 2) +Math.Pow(2, 4)  + Math.Pow(2, 8)),
                            GoodMask = (int)(Math.Pow(2, 3)  + Math.Pow(2, 6)),
                            FairlyMask = (int)(Math.Pow(2, 9)),
                            LowMask = (int)(Math.Pow(2, 1)  +Math.Pow(2, 5)+ Math.Pow(2, 7))}; 
                        };
         if (Id==3)
                        {   return new PersonalityNumber() {
                            Id = 3,
                            HighMask = (int)(Math.Pow(2, 3) +Math.Pow(2, 6)  + Math.Pow(2, 9)),
                            GoodMask = (int)(Math.Pow(2, 1)  +Math.Pow(2, 2)+ Math.Pow(2, 5)),
                            FairlyMask = 0,
                            LowMask = (int)(Math.Pow(2, 4)  +Math.Pow(2, 7)+ Math.Pow(2, 8))}; 
                        };
         if (Id==4)
                        {   return new PersonalityNumber() {
                            Id = 4,
                            HighMask = (int)(Math.Pow(2, 2) +Math.Pow(2, 4)  + Math.Pow(2, 8)),
                            GoodMask = (int)(Math.Pow(2, 6) + Math.Pow(2, 7)),
                            FairlyMask = 0,
                            LowMask = (int)(Math.Pow(2, 1)  +Math.Pow(2, 3)+  +Math.Pow(2, 5) + Math.Pow(2, 9))}; 
                        };
           if (Id==5)
                        {   return new PersonalityNumber() {
                            Id = 5,
                            HighMask = (int)(Math.Pow(2, 1) +Math.Pow(2, 5)  + Math.Pow(2, 7)),
                            GoodMask = (int)(Math.Pow(2, 3)  + Math.Pow(2, 9)),
                            FairlyMask = (int)(Math.Pow(2, 8)),
                            LowMask = (int)(Math.Pow(2, 2)  +Math.Pow(2, 4)+ Math.Pow(2, 6))}; 
                        };
          if (Id==6)
                        {   return new PersonalityNumber() {
                            Id = 6,
                            HighMask = (int)(Math.Pow(2, 3) +Math.Pow(2, 6)  + Math.Pow(2, 9)),
                            GoodMask = (int)(Math.Pow(2, 2)  +Math.Pow(2, 4)+ Math.Pow(2, 8)),
                            FairlyMask = 0,
                            LowMask = (int)(Math.Pow(2, 1)  +Math.Pow(2, 5)+ Math.Pow(2, 7))}; 
                        };
           if (Id==7)
                        {   return new PersonalityNumber() {
                            Id = 7,
                            HighMask = (int)(Math.Pow(2, 1) +Math.Pow(2, 5)  + Math.Pow(2, 7)),
                            GoodMask = (int)(Math.Pow(2, 4)),
                            FairlyMask = (int)(Math.Pow(2, 9)),
                            LowMask = (int)(Math.Pow(2, 2)  +Math.Pow(2, 3)+  Math.Pow(2, 6) + Math.Pow(2, 8))}; 
                        };
            if (Id==8)
                        {   return new PersonalityNumber() {
                            Id = 8,
                            HighMask = (int)(Math.Pow(2, 2) +Math.Pow(2, 3)  + Math.Pow(2, 8)),
                            GoodMask = (int)(Math.Pow(2, 6)),
                            FairlyMask = (int)(Math.Pow(2, 1) + Math.Pow(2, 5)),
                            LowMask = (int)(Math.Pow(2, 3)  +Math.Pow(2, 7) + Math.Pow(2, 9))}; 
                        };
           if (Id==9)
                        {   return new PersonalityNumber() {
                            Id = 9,
                            HighMask = (int)(Math.Pow(2, 3) +Math.Pow(2, 6)  + Math.Pow(2, 9)),
                            GoodMask = (int)(Math.Pow(2, 1) + Math.Pow(2, 5)),
                            FairlyMask = (int)(Math.Pow(2, 2) + Math.Pow(2, 7)),
                            LowMask = (int)(Math.Pow(2, 4)  + Math.Pow(2, 8))}; 
                        };

            return   new PersonalityNumber() {Id=0}; 
        }

//-------------------------------------------------------------
        public static int GetPersonalityNumberId(DateTime date)
        {
                char[] a = date.Year.ToString().ToCharArray();
                int m = GetNumber(date.Month.ToString().ToCharArray().Sum(p => Int32.Parse(p.ToString())));
                int y = GetNumber(date.Year.ToString().ToCharArray().Sum(p => Int32.Parse(p.ToString())));
                int d = GetNumber(date.Day.ToString().ToCharArray().Sum(p => Int32.Parse(p.ToString())));
                int sum = GetNumber(m + y + d);
            return sum;

        }

//---------------------------------------------------
        private static int GetNumber(int n)
        {
            do
            {
                n = n.ToString().
                       ToCharArray().AsEnumerable()
                          .Sum(nn => Int32.Parse(nn.ToString()));
            } while (n > 9);
            return n;
        }

    }
   
}

       


