﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Match.Domain.Abstract;

namespace Match.Domain.Entities
{
    //What was your total household income in 2014 from all sources before taxes?
    public class Income : EntityBase<int>, IAttribute
    {
        public string Name { get; set; }
     //   public virtual ICollection<Profile> Profiles { get; set; }
        public static string[] InitValue ={ "Less than $15,000","$15,001 - $25,000","$25,001 - $35,000","$15,001 - $25,000","$35,001 - $45,000",
"$45,001 - $55,000",
"$55,001 - $65,000",
"$65,001 - $75,000",
"$75,001 - $85,000",
"$85,001 - $90,000",
"$90,001 - $95,000",
"$95,001 - $100,000",
"$100,001 - $125,000",
"$125,001 - $150,000",
"$150,001 - $200,000",
"$200,001 - $300,000",
"$300,001 - $400,000",
"Over $400,000"
};
    }
}