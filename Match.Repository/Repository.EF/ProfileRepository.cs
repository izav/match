﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using Match.Repository.Abstract;
using Match.Domain.Entities;
using Match.Repository.ContextStorage;
using Match.Domain.Abstract;

using System.Web.Mvc;

using System.Data.Entity.Validation;

//EntityFramework.dll (has DbContext API, Code First)
//System.Data.Entity.dll (has ObjectContext and DbContext APIs, new features)

namespace Match.Repository.EF
{
    public class ProfileRepository : IRepositoryCRUD<Profile, int>
    {
        private DbContextEF context;
        DbSet<Profile> set;

        public ProfileRepository()
        {
            context = DataContextFactory.GetDataContext();
            set = context.Profiles;
        }

        // DbContext
        public string GetEntitySetName()
        {
            return "Profiles";
        }

        public IQueryable<Profile> GetDBSet()
        {
            
            return set;
        }
       

        //Find All products
        //--------------------------------
        public IQueryable<Profile> FindAll()
        {
            return GetDBSet();
        }


       
        

        //--------------------------------------------------
        public DateTime GetDateBirth(int id)
        {
            Profile profile = context.Profiles.Find(id);
            if (profile == null) return DateTime.MinValue;
            else return profile.DateBirth;
        
        }
        //------------------------------------------------------------------------------------
        public IQueryable<Profile> FindAllByNumerology(IQueryable<Profile> p, int PersonalityNumberId)
        {       
            
           PersonalityNumber pp = PersonalityNumber.GetPersonalityNumber(PersonalityNumberId);
             int userMask = pp.HighMask | pp.GoodMask;

             return p.Where(s => ((int)(Math.Pow(2, s.PersonalityNumberId)) & userMask) > 0);
        }


        //SEARCH 
        //------------------------u------------------------------
      public IQueryable<Profile> FindByQuery (Profile user)
        {
            int userAgeId = user.AgeFromDateBirth - 17; 
           
            IQueryable<Profile> result = context.Profiles
                .Where(p =>
                            p.Id != user.Id &&
                    //Gender 
                            p.GenderId == user.LookingForId &&
                            p.LookingForId == user.GenderId &&

                            // Age   FromTo
                         /*      p.SearchAgeIdFrom <= userAgeId &&
                            p.SearchAgeIdTo >= userAgeId &&
                            user.SearchAgeIdFrom <= (p.AgeFromDateBirth - 17) &&
                            user.SearchAgeIdTo >= (p.AgeFromDateBirth - 17)//&& */
 

                            // Height   FromTo
                            p.SearchHeightIdFrom <= user.HeightId &&
                            p.SearchHeightIdTo >= user.HeightId &&
                            user.SearchHeightIdFrom <= p.HeightId &&
                            user.SearchHeightIdTo >= p.HeightId)// &&


                            // Weight   FromTo
                    /*        p.SearchWeightIdFrom <= user.WeightId &&
                            p.SearchWeightIdTo >= user.WeightId &&
                            user.SearchWeightIdFrom <= p.WeightId &&
                            user.SearchWeightIdTo >= p.WeightId &&


                             // Income   FromTo
                            p.SearchIncomeIdFrom <= user.IncomeId &&
                            p.SearchIncomeIdTo >= user.IncomeId &&
                            user.SearchIncomeIdFrom <= p.IncomeId &&
                            user.SearchIncomeIdTo >= p.IncomeId &&

                             // Children   FromTo
                            p.SearchChildrenIdFrom <= user.ChildrenId &&
                            p.SearchChildrenIdTo >= user.ChildrenId &&
                            user.SearchChildrenIdFrom <= p.ChildrenId &&
                            user.SearchChildrenIdTo >= p.ChildrenId &&

                           
                            //Religion  OR
                             ((long)(Math.Pow(2, p.ReligionId)) & user.SearchReligionIdMask) > 0 &&
                             ((long)(Math.Pow(2, user.ReligionId)) & p.SearchReligionIdMask) > 0 &&
                     
                             //Status  OR
                             ((long)(Math.Pow(2, p.StatusId)) & user.SearchStatusIdMask) > 0 &&
                             ((long)(Math.Pow(2, user.StatusId)) & p.SearchStatusIdMask) > 0)// &&
                      
                             //Occupation OR
                          //   ((long)(Math.Pow(2, p.OccupationId)) & user.SearchOccupationIdMask) > 0 &&
                          //   ((long)(Math.Pow(2, user.OccupationId)) & p.SearchOccupationIdMask) > 0)// &&

                             //Education OR
                         //    ((long)(Math.Pow(2, p.EducationGradeId)) & user.SearchEducationGradeIdMask) > 0 &&
                          //   ((long)(Math.Pow(2, user.EducationGradeId)) & p.SearchEducationGradeIdMask) > 0)// &&

                             //Smoke OR
                          //   ((long)(Math.Pow(2, p.SmokeId)) & user.SearchSmokeIdMask) > 0 &&
                           //  ((long)(Math.Pow(2, user.SmokeId)) & p.SearchSmokeIdMask) > 0)// &&

                             //Race OR
                          /*   ((long)(Math.Pow(2, p.RaceId)) & user.SearchRaceIdMask) > 0 &&
                             ((long)(Math.Pow(2, user.RaceId)) & p.SearchRaceIdMask) > 0 &&

                             //Disability OR
                             ((long)(Math.Pow(2, p.DisabilityId)) & user.SearchDisabilityIdMask) > 0 &&
                             ((long)(Math.Pow(2, user.DisabilityId)) & p.SearchDisabilityIdMask) > 0)*/

                             .Include("ProfileImages")
                             .Include("State")
                             .Include("Country")

                             .Include("Status")
                             .Include("EducationGrade")
                             .Include("Race")
                             .Include("Occupation")
                             .Include("Religion")
                             .Include("Disability")
                             .Include("Smoke")
                             .Include("Income")
                             .Include("Children")
                             .Include("Height")
                             .Include("Weight");
                            
            return result;
        }

          // SEARCH  - take users Ids if ZERO
        //---------------------------------------------
        public ResponseModel FindByQuery(int id,  bool useNumerology, bool useZodiac)
        {
            Profile user = context.Profiles.Find(id);
            if (user == null) return null;
           

            ResponseModel response = new ResponseModel();
            response.Profile = user;

            //simple search
            response.Profiles = FindByQuery(user); 
            response.AllProfiles = response.Profiles.Count();
            PersonalityNumber p = PersonalityNumber.GetPersonalityNumber(user.PersonalityNumberId);
            int userMask = p.HighMask | p.GoodMask;

            //use Numerology
            if (useNumerology)
            {                 
                response.Profiles = response.Profiles.Where(s => 
                    ((int)(Math.Pow(2 ,s.PersonalityNumberId)) & userMask)  > 0);
                response.AllProfilesByNumerology = response.Profiles.Count();
                response.AllProfiles = response.AllProfilesByNumerology;
            }
            
           
            return response;            
        }

        //------------------------------------------------------
        public NumeralogyNumber GetNumeralogyInfoBy(int n1, int n2)
        {
            NumeralogyNumber n = context.NumeralogyNumbers
                .Where(p => p.Number1 == n1 && p.Number2 == n2)
                .FirstOrDefault(); 
            return n;
        }


        //---------------------------------------------------

        private IEnumerable<T> GetAttributeSetByMask<T>(long mask, IEnumerable<T> source)
                                                      where T : class, IAttribute, new()
        {
            
            IEnumerable<T> result = null;
            if (mask != 0)
            {
                    result = context.Set<T>()
                    .Where(p => ((long)(Math.Pow(2, p.Id)) & mask) > 0)
                    .Select(p => p);
            }

            if (result == null || result.Count() == 0)
            {
                 List<T> zeroResult = new List<T>();
                 zeroResult.Add(new T() { Id = 0, Name = "Not selected" });
                 return zeroResult;
            }
            return result;
        
        }

        //---------------------------------------------------------
        public ProfileAndSearchView GetProfileAndCriteriaBy(int id)
        {
            ProfileAndSearchView pd = new ProfileAndSearchView();
            Profile p = FindBy(id, true);
            pd.Profile = p;
            if (p == null) return pd;
            pd.SearchDisabilities = GetAttributeSetByMask( p.SearchDisabilityIdMask,
                                        context.Set<Disability>()).ToList();
            pd.SearchStatuses = GetAttributeSetByMask(p.SearchStatusIdMask,
                                       context.Set<Status>()).ToList();
            pd.SearchSmokes = GetAttributeSetByMask(p.SearchSmokeIdMask,
                                       context.Set<Smoke>()).ToList();
            pd.SearchOccupations = GetAttributeSetByMask(p.SearchOccupationIdMask,
                                       context.Set<Occupation>()).ToList();
            pd.SearchRaces = GetAttributeSetByMask(p.SearchRaceIdMask,
                                       context.Set<Race>()).ToList();
            pd.SearchReligions = GetAttributeSetByMask(p.SearchReligionIdMask,
                                       context.Set<Religion>()).ToList();
            pd.SearchEducationGrades = GetAttributeSetByMask(p.SearchEducationGradeIdMask,
                                       context.Set<EducationGrade>()).ToList();

            return pd;
        
        }

        //--------------------------------------------------
        public Profile FindBy(int id, bool isFullInfo = false)
        {
            {
               
                if (isFullInfo)
                    return context.Profiles
                     .Where(p => p.Id == id)
                     .Include("ProfileImages")
                     .Include("State")
                     .Include("Country")

                    .Include("Status")
                    .Include("EducationGrade")
                    .Include("Race")
                    .Include("Occupation")
                    .Include("Religion")
                    .Include("Disability")
                    .Include("Smoke")

                   // .Include("State")
                  //  .Include("Country")

                    .Include("SearchAgeFrom")
                    .Include("SearchAgeTo")

                    .Include("Income")
                    .Include("SearchIncomeFrom")
                    .Include("SearchIncomeTo")

                    .Include("Children")
                    .Include("SearchChildrenFrom")
                    .Include("SearchChildrenTo")

                    .Include("Height")
                    .Include("SearchHeightFrom")
                    .Include("SearchHeightTo")

                    .Include("Weight")
                    .Include("SearchWeightFrom")
                    .Include("SearchWeightTo")
                    .FirstOrDefault();

                  

                else
                return context.Profiles
                    .Where(p => p.Id == id)
                    .Include("ProfileImages")
                    .FirstOrDefault();
            }
        }


        //-------------------------u------------------------------------------------------
        public void SaveProfileInfo(Profile entity, int mainImageOrderId)
        {
            Profile profile = context.Profiles
                                     .Find(entity.Id) ?? context.Profiles.Add(entity);

            entity.LookingForId = entity.GenderId == 1 ? 2 : 1;
            // new item
            if (profile == entity)
            {
                profile.PersonalityNumberId = PersonalityNumber.GetPersonalityNumberId(profile.DateBirth);
                profile.ZodiacSignId = ZodiacSign.GetZodiacSignId(profile.DateBirth);
                context.Entry(profile).State = EntityState.Added;
            }
            else //old item
            {

                

                if (entity.DateBirth != profile.DateBirth)
                {
                    entity.PersonalityNumberId = PersonalityNumber.GetPersonalityNumberId(entity.DateBirth);
                    entity.ZodiacSignId = ZodiacSign.GetZodiacSignId(entity.DateBirth);
                }
                else
                {
                    entity.PersonalityNumberId = profile.PersonalityNumberId;
                    entity.ZodiacSignId = profile.ZodiacSignId;
                }
                  

                context.Entry(profile).CurrentValues.SetValues(entity);

                //-------------      Images      -------------------------            

                // Has Images to update
                if (entity.ProfileImages != null)
                {
                    context.Entry(profile).Collection(p => p.ProfileImages)
                                     .Load();

                    var imagesForUpdate =
                        from p1 in entity.ProfileImages
                        join p2 in profile.ProfileImages on p1.Id equals p2.Id
                        where p1.ImageOrderId != p2.ImageOrderId
                        select new { ProductTitle = p2, ImageOrderId = p1.ImageOrderId };



                    foreach (var p in imagesForUpdate)
                    {
                        p.ProductTitle.ImageOrderId = p.ImageOrderId;
                    }

                    //ImagesForDelete
                    var imagesForDelete =
                                             (from p in profile.ProfileImages
                                              join e in entity.ProfileImages on p.Id equals e.Id into pe
                                              from s in pe.DefaultIfEmpty()
                                              select new { ImageFromDB = p, ImageId = (s == null ? 0 : s.Id) })
                                             .Where(k => k.ImageId == 0)
                                             .Select(n => n.ImageFromDB);



                    foreach (var item in imagesForDelete.ToList())
                        context.Entry(item).State = EntityState.Deleted;

                    // ImagesForInsert
                    var imagesSetForInsert = entity.ProfileImages
                                                 .Where(p => p.Id == 0);

                    foreach (var q in imagesSetForInsert.ToList())
                    {
                        profile.ProfileImages.Add(q);
                    }
                }
                else
                {        // All image to delete
                    if (profile.ProfileImages != null && profile.ProfileImages.Count() > 0)
                    {

                        foreach (var item in profile.ProfileImages.ToList())
                        {
                            context.Entry(item).State = EntityState.Deleted;
                        }
                    }
                } // images

                context.Entry(profile).State = EntityState.Modified;
            }//else old
            

           
            context.SaveChanges();

            //MainProfileImageId 
            Profile pr = context.Profiles
                          .Where(k => k.Id == profile.Id).Include("ProfileImages").FirstOrDefault();
            ProfileImage image = pr.ProfileImages.Where(cc => cc.ImageOrderId == mainImageOrderId).FirstOrDefault();

            if (image == null)
            {
                image = pr.ProfileImages.Where(cc => cc.ImageOrderId == 1).FirstOrDefault();
                if (image == null) pr.MainProfileImageId = 0;
                else pr.MainProfileImageId = image.Id;
            }
            else pr.MainProfileImageId = image.Id;

            context.Entry(pr).State = EntityState.Modified;
            context.SaveChanges();
           
        
        }
       

        //---------------------------u-------------------------------
        public ResponseModel FindAllFor(int id, bool giveAllForNew)
        {

            ResponseModel response = new ResponseModel();
            Profile p = context.Profiles.Find(id) ?? new Profile();

            if (p.Id == 0) // new
            {
                p.DateBirth = DateTime.MinValue;
                if (!giveAllForNew) return response;
                p.Id = id; 
            }
            else
               context.Entry(p).Collection("ProfileImages").Load(); // old
              
              response.Profile = p;
               
              response.Statuses = context.Statuses;
              response.Religions = context.Religions;
              response.Countries = context.Countries;
              response.States = context.States;
              response.Heights = context.Heights;
              response.Weights = context.Weights;
              response.Smokes = context.Smokes;
              response.Ages = context.Ages;
              response.HasChildren = context.HasChildren;
              response.Disabilities = context.Disabilities;
              response.EducationGrades = context.EducationGrades;
              response.Incomes = context.Incomes;
              response.Races = context.Races;
              response.Occupations = context.Occupations;

              return response;
        }
       



        //--------------------------------------------------------------
        public ProfileImage FindMainProfileImageBy( int id, int imageId=0)
        {
            ProfileImage image = null;
            if (imageId != 0) return context.ProfileImages.Find(imageId);
            else
            {
                Profile p = context.Profiles
                            .Where(pp => pp.Id==id).Include("ProfileImages").FirstOrDefault();
                if (p == null || p.ProfileImages == null) return null;

                image = p.ProfileImages.Where(cc => cc.ImageOrderId == 1).FirstOrDefault();
                if (image == null) return null;
                // save main image Id if  0
                p.MainProfileImageId = image.Id;
                context.Entry(p).State = EntityState.Modified;
                context.SaveChanges();
            }
            return image;

        }

        //--------------------------------------------------------------
        public ProfileImage FindProfileImageBy(int imageId)
        {
            return context.ProfileImages.Find(imageId);

        }
        //--------------------------------------------------------------------
        public void Remove(int id)
        {
            Profile entity = GetDBSet().FirstOrDefault<Profile>(b => b.Id == id);
            context.Entry(entity).State = EntityState.Deleted;
            context.SaveChanges();
        }

        //---------------------------------------------------------------------
        public void Add(Profile entity)
        {
            Profile c = entity;
            context.Entry(c).State = EntityState.Added;
            context.SaveChanges();

        }
       //------------------------------------------------------------

        private void SaveChanges(DbContext context)
        {
            try
            {
                context.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                StringBuilder sb = new StringBuilder();

                foreach (var failure in ex.EntityValidationErrors)
                {
                    sb.AppendFormat("{0} failed validation\n", failure.Entry.Entity.GetType());
                    foreach (var error in failure.ValidationErrors)
                    {
                        sb.AppendFormat("- {0} : {1}", error.PropertyName, error.ErrorMessage);
                        sb.AppendLine();
                    }
                }

                throw new DbEntityValidationException(
                    "Entity Validation Failed - errors follow:\n" +
                    sb.ToString(), ex
                ); 
            }
        }
    }
}

