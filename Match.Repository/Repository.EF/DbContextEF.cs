﻿using System.Data.Entity;
//using System.Data.Objects;
using System.Data.Entity.Infrastructure;
using Match.Domain.Entities;
using Match.Domain.Abstract;

using System.ComponentModel.DataAnnotations.Schema;

namespace Match.Repository.EF
{
    public class DbContextEF : DbContext
    {
        public DbSet<ExternalUserInformation> ExternalUsers { get; set; }
        public DbSet<UserProfile> UserProfiles { get; set; }
         public DbSet<Criteria> Criterias { get; set; }
       

        public DbSet<NumeralogyNumber> NumeralogyNumbers { get; set; }
        public DbSet<Profile> Profiles { get; set; }
        public DbSet<ProfileImage> ProfileImages {get; set;}
        public DbSet<Smoke> Smokes { get; set; }
        public DbSet<EducationGrade> EducationGrades { get; set; }
        public DbSet<Status> Statuses { get; set; }
        public DbSet<Religion> Religions { get; set; }
        public DbSet<Disability> Disabilities { get; set; }
        public DbSet<State> States { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<Height> Heights { get; set; }
        public DbSet<SearchHeight> SearchHeights { get; set; }
        public DbSet<Weight> Weights { get; set; }
        public DbSet<SearchWeight> SearchWeights { get; set; }
        public DbSet<Age> Ages { get; set; }
        public DbSet<SearchAge> SearchAges { get; set; }
        public DbSet<Children> HasChildren { get; set; }
        public DbSet<Occupation> Occupations { get; set; }
        public DbSet<Race> Races { get; set; }
        public DbSet<Income> Incomes { get; set; }


      //--------------------------------------------------
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            modelBuilder.Entity<Profile>()
            .Map(m =>
            {
                m.MapInheritedProperties();
                m.ToTable("Profiles");
            });

          
         modelBuilder.Entity<Profile>()
                .HasKey(e => e.Id);


         // FK   
       /*   modelBuilder.Entity<UserProfile>()
                        .HasOptional(s => s.Profile) // Mark StudentAddress is optional for Student
                        .WithRequired(ad => ad.UserProfile); // Create inverse relationship
                      
           */

            //CascadeOnDelete
            //----------------------
            modelBuilder.Entity<Profile>()
                .HasRequired(m => m.Disability)
                .WithMany()
                .HasForeignKey(m => m.DisabilityId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Profile>()
                        .HasRequired(m => m.Smoke)
                        .WithMany()
                        .HasForeignKey(m => m.SmokeId)
                        .WillCascadeOnDelete(false);

            modelBuilder.Entity<Profile>()
                        .HasRequired(m => m.EducationGrade)
                        .WithMany()
                        .HasForeignKey(m => m.EducationGradeId)
                        .WillCascadeOnDelete(false);

            modelBuilder.Entity<Profile>()
                        .HasRequired(m => m.Religion)
                        .WithMany()
                        .HasForeignKey(m => m.ReligionId)
                        .WillCascadeOnDelete(false);

            modelBuilder.Entity<Profile>()
                        .HasRequired(m => m.Status)
                        .WithMany()
                        .HasForeignKey(m => m.StatusId)
                        .WillCascadeOnDelete(false);

            modelBuilder.Entity<Profile>()
                        .HasRequired(m => m.EducationGrade)
                        .WithMany()
                        .HasForeignKey(m => m.EducationGradeId)
                        .WillCascadeOnDelete(false);


            modelBuilder.Entity<Profile>()
                       .HasRequired(m => m.Race)
                       .WithMany()
                       .HasForeignKey(m => m.RaceId)
                       .WillCascadeOnDelete(false);

            modelBuilder.Entity<Profile>()
                      .HasRequired(m => m.Occupation)
                      .WithMany()
                      .HasForeignKey(m => m.OccupationId)
                      .WillCascadeOnDelete(false);



            modelBuilder.Entity<Profile>()
                       .HasRequired(m => m.Income)
                       .WithMany()
                       .HasForeignKey(m => m.IncomeId)
                       .WillCascadeOnDelete(false);
            modelBuilder.Entity<Profile>()
                      .HasRequired(m => m.SearchIncomeFrom)
                      .WithMany()
                      .HasForeignKey(m => m.SearchIncomeIdFrom)
                      .WillCascadeOnDelete(false);
            modelBuilder.Entity<Profile>()
                      .HasRequired(m => m.SearchIncomeTo)
                      .WithMany()
                      .HasForeignKey(m => m.SearchIncomeIdTo)
                      .WillCascadeOnDelete(false);


            modelBuilder.Entity<Profile>()
                    .HasRequired(m => m.SearchAgeFrom)
                    .WithMany()
                    .HasForeignKey(m => m.SearchAgeIdFrom)
                    .WillCascadeOnDelete(false);
            modelBuilder.Entity<Profile>()
                      .HasRequired(m => m.SearchAgeTo)
                      .WithMany()
                      .HasForeignKey(m => m.SearchAgeIdTo)
                      .WillCascadeOnDelete(false);


            modelBuilder.Entity<Profile>()
                   .HasRequired(m => m.SearchHeightFrom)
                   .WithMany()
                   .HasForeignKey(m => m.SearchHeightIdFrom)
                   .WillCascadeOnDelete(false);
            modelBuilder.Entity<Profile>()
                      .HasRequired(m => m.SearchHeightTo)
                      .WithMany()
                      .HasForeignKey(m => m.SearchHeightIdTo)
                      .WillCascadeOnDelete(false);


            modelBuilder.Entity<Profile>()
                   .HasRequired(m => m.SearchWeightFrom)
                   .WithMany()
                   .HasForeignKey(m => m.SearchWeightIdFrom)
                   .WillCascadeOnDelete(false);
            modelBuilder.Entity<Profile>()
                      .HasRequired(m => m.SearchWeightTo)
                      .WithMany()
                      .HasForeignKey(m => m.SearchWeightIdTo)
                      .WillCascadeOnDelete(false);


            modelBuilder.Entity<Profile>()
                  .HasRequired(m => m.SearchChildrenFrom)
                  .WithMany()
                  .HasForeignKey(m => m.SearchChildrenIdFrom)
                  .WillCascadeOnDelete(false);
            modelBuilder.Entity<Profile>()
                      .HasRequired(m => m.SearchChildrenTo)
                      .WithMany()
                      .HasForeignKey(m => m.SearchChildrenIdTo)
                      .WillCascadeOnDelete(false);



           

           //--------------------------------

            modelBuilder.Entity<ProfileImage>()
            .Map(m =>
            {
                m.MapInheritedProperties();
                m.ToTable("ProfileImages");
            });



            modelBuilder.Entity<Status>()
            .Map(m =>
            {
                m.MapInheritedProperties();
                m.ToTable("Statuses");
            });


            modelBuilder.Entity<Country>().Map(m =>
            {
                m.MapInheritedProperties();
                m.ToTable("Countries");
            });

            modelBuilder.Entity<State>().Map(m =>
            {
                m.MapInheritedProperties();
                m.ToTable("States");
            });
            

            modelBuilder.Entity<Height>().Map(m =>
            {
                m.MapInheritedProperties();
                m.ToTable("Heights");
            });

            modelBuilder.Entity<Weight>().Map(m =>
            {
                m.MapInheritedProperties();
                m.ToTable("Weights");
            });

            modelBuilder.Entity<SearchAge>().Map(m =>
            {
                m.MapInheritedProperties();
                m.ToTable("SearchAges");
            });

            
            modelBuilder.Entity<Children>().Map(m =>
            {
                m.MapInheritedProperties();
                m.ToTable("Children");
            });
            modelBuilder.Entity<Disability>().Map(m =>
            {
                m.MapInheritedProperties();
                m.ToTable("Disabilities");
            });

           

            modelBuilder.Entity<Occupation>().Map(m =>
            {
                m.MapInheritedProperties();
                m.ToTable("Occupations");
            });


            //-----------------------------------------
            modelBuilder.Entity<EducationGrade>().Map(m =>
            {
                m.MapInheritedProperties();
                m.ToTable("EducationGrades");
            });



            //------------------------------------------
            modelBuilder.Entity<Smoke>().Map(m =>
              {
                  m.MapInheritedProperties();
                  m.ToTable("Smokes");
              });

            //---------------------------------------------------
            // Identity

            //------------------------------------------------
            modelBuilder.Entity<EducationGrade>()
             .Property(e => e.Id)
             .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);


            modelBuilder.Entity<Smoke>()
            .Property(e => e.Id)
            .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

           

            modelBuilder.Entity<Status>()
                  .Property(e => e.Id)
                  .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<ProfileImage>()
                .Property(e => e.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
 
            modelBuilder.Entity<Religion>()
                   .Property(e => e.Id)
                   .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<Country>()
                  .Property(e => e.Id)
                  .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<State>()
                  .Property(e => e.Id)
                  .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

           modelBuilder.Entity<Height>()
              .Property(e => e.Id)
              .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

           modelBuilder.Entity<Weight>()
              .Property(e => e.Id)
              .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<SearchAge>()
              .Property(e => e.Id)
              .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

           modelBuilder.Entity<Disability>()
              .Property(e => e.Id)
              .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
           

            modelBuilder.Entity<Occupation>()
             .Property(e => e.Id)
             .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);


            modelBuilder.Entity<Race>()
             .Property(e => e.Id)
             .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<Income>()
            .Property(e => e.Id)
            .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

        }
    }
}
