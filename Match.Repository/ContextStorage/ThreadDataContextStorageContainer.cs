﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Threading;
//using System.Data.Objects;
using System.Data.Entity.Infrastructure;
using Match.Repository.EF;

namespace Match.Repository.ContextStorage
{
    public class ThreadDataContextStorageContainer : IDataContextStorageContainer 
    {    
        private static readonly Hashtable _storeDataContexts = new Hashtable();

        public DbContextEF GetDataContext()
        {
            DbContextEF storeDataContext = null;

            if (_storeDataContexts.Contains(GetThreadName()))
                storeDataContext = (DbContextEF)_storeDataContexts[GetThreadName()];           

            return storeDataContext;
        }

        public void Store(DbContextEF libraryDataContext)
        {
            if (_storeDataContexts.Contains(GetThreadName()))
                _storeDataContexts[GetThreadName()] = libraryDataContext;
            else
                _storeDataContexts.Add(GetThreadName(), libraryDataContext);           
        }

        private static string GetThreadName()
        {
            return Thread.CurrentThread.Name;
        }  
   
        
    }
}
