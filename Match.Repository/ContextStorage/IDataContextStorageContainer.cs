﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Match.Repository.EF;

namespace Match.Repository.ContextStorage
{
    public interface IDataContextStorageContainer
    {
        DbContextEF GetDataContext();
        void Store(DbContextEF storeDataContext);
       
    }
}
