﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;

namespace Match.WebUI.ViewModel
{
    public class SimpleMatching
    {
         [DisplayName("Apply to matching:")]
        public int[] ApplyToMatching { get; set; }

         [DisplayName("First Date of Birth")]
        public DateTime DOB1 { get; set; }
          [DisplayName("Second Date of Birth")]
        public DateTime DOB2 { get; set; }
    }
}