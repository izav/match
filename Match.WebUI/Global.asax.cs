﻿using System;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Match.WebUI.Infrastructure;
using System.Web.Security;
using WebMatrix.WebData;
using Match.Domain.Entities;
using Match.Repository.EF;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using Match.Domain.Abstract;
using System.ComponentModel.DataAnnotations.Schema;

namespace Match.WebUI
{
    // API Key (Secret) /689877bf907b58b16fa623893d22ff8faf54d674
    // Application Domain  https://izn.rpxnow.com/
    //App ID beikgmjdbecdjenahkip
    // slonixa@68.yahoo.com - irinaSLONIXA  irina@@( na JANRIAN https://dashboard.janrain.com)


    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AuthConfig.RegisterAuth();
            ControllerBuilder.Current.SetControllerFactory(new NinjectControllerFactory());

            //ira

            Database.SetInitializer<DbContextEF>(new ContextInitializerEF());
            using (DbContextEF context = new DbContextEF())
            {
                context.Database.Initialize(false);
                if (!WebSecurity.Initialized)
                    WebSecurity.InitializeDatabaseConnection("DbContextEF",
                         "UserProfile", "UserId", "UserName", autoCreateTables: true);
                
            }
            

        }
    }
}