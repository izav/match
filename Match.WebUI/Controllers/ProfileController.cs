﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Match.Repository.Abstract;
using Match.Domain.Entities;
using Match.Repository.EF;
using Microsoft.Web.WebPages.OAuth;
using WebMatrix.WebData;
using Match.WebUI.ViewModel;



namespace Match.WebUI.Controllers
{
   [Authorize] 
    public class ProfileController : Controller
    {
        private readonly IRepositoryCRUD<Profile, int> repository;

        //--------------------u---------------------------------------
        public ProfileController(IRepositoryCRUD<Profile, int> repository)
            
        {
            this.repository = repository;
        }

   
     //------------------------------u-------------------------------------
       public FileContentResult GetProfileImage(int imageId)
       {

           ProfileImage f = repository.FindProfileImageBy( imageId);
           if (f == null || f.ImageData == null) return null;
           return File(f.ImageData, f.ImageMimeType);

       }
       //----------------------------------------------------------
      public  FileContentResult GetMainProfileImage(int imageId)
       {
           var id = WebSecurity.CurrentUserId;
           ProfileImage f = ((ProfileRepository)repository).FindMainProfileImageBy(id, imageId);
           if (f == null || f.ImageData == null) return null;
           return File(f.ImageData, f.ImageMimeType);

       }

      


        //-------------------u---------------
       [HttpGet]
       public ActionResult ProfileInfo()
        {
            var id = WebSecurity.CurrentUserId;
            ResponseModel responseModel = repository.FindAllFor(id, true);
            return View("ProfileInfo", responseModel);
        }
      //-----------------u---------------------------------
       private static long GetMaskFromArray(int[] arr)
       {
           return arr == null ? 0 :  arr.Sum(s => (long)(Math.Pow(2, s)));
       }

        
     //----------------------------------u------------------------------
       [HttpPost]
      // [ValidateAntiForgeryToken]
       public ActionResult ProfileInfo(Profile Profile, 
           IEnumerable<HttpPostedFileBase> file, int[] fileId, int[] mainImage, int hasImages=0)
       {
           var id = WebSecurity.CurrentUserId;

           Profile.SearchDisabilityIdMask = GetMaskFromArray (Profile.SearchDisabilityIds);
           Profile.SearchSmokeIdMask = GetMaskFromArray (Profile.SearchSmokeIds);
           Profile.SearchEducationGradeIdMask = GetMaskFromArray (Profile.SearchEducationGradeIds);
           Profile.SearchOccupationIdMask = GetMaskFromArray(Profile.SearchOccupationIds);
           Profile.SearchRaceIdMask = GetMaskFromArray(Profile.SearchRaceIds);
           Profile.SearchStatusIdMask = GetMaskFromArray(Profile.SearchStatusIds);
           Profile.SearchReligionIdMask = GetMaskFromArray(Profile.SearchReligionIds);

           int  mainImageOrderId = 0;

           if (ModelState.IsValid)           
            {
                Profile.Id = id;
               

               //---------------------------------------
                //if (hasImages == 1)
               if (file != null && file.Count()> 0)
                 {

                     mainImageOrderId = Array.IndexOf(mainImage, 1) + 1;

                     var imageFiles = file.Select((image, i) => new
                     {

                         ImageOrderId = i + 1,
                         Image = image,
                         ProfileImage = (image == null ? null : new ProfileImage()
                         {
                             Name = "",
                             ImageOrderId = i + 1,
                             ImageMimeType = image.ContentType,
                             ImageData = new byte[image.ContentLength]
                         })
                     }).ToList();

                     foreach (var item in imageFiles)
                     {
                         if (item.Image != null)
                             item.Image.InputStream.Read(item.ProfileImage.ImageData, 0, item.Image.ContentLength);
                     }

                    
         

                     var imageIds = fileId.Select((p, i) => new { ImageOrderId = i + 1, ImageId = p });
                     Profile.ProfileImages = imageFiles
                                     .Join(imageIds,
                                      f => f.ImageOrderId,
                                      fId => fId.ImageOrderId,
                                      (f, fId) =>
                                          new ProfileImage()
                                          {
                                              Id = fId.ImageId,
                                              ImageOrderId = f.ImageOrderId,
                                              ImageData = (f.Image == null ? null : f.ProfileImage.ImageData),
                                              ImageMimeType = (f.Image == null ? "" : f.ProfileImage.ImageMimeType)
                                          }).ToList();
                 }
                 else
                 {
                     Profile.ProfileImages = null;
                 }



                repository.SaveProfileInfo(Profile, mainImageOrderId);
                
                return RedirectToAction("Search",new {isOpen=true});
               //----------------------------------
            }
            else
            {
               ResponseModel responseModel = repository.FindAllFor(id, true);
               responseModel.Profile = Profile;

               if (fileId != null && fileId.Count() > 0)
               {
                   responseModel.Profile.ProfileImages = fileId
                       .Where(p => p > 0)
                       .Select(s => new ProfileImage() { Id = s })
                       .ToList();
               }
                 
               return View("ProfileInfo", responseModel);
            }
        }
   
        //-----------------u------------------------
       [HttpGet]
        public ActionResult Criteria()
        {
            var id = WebSecurity.CurrentUserId;
            
            ResponseModel responseModel = repository.FindAllFor(id, false);
            if (responseModel.Profile == null) return View("CriteriaDenial");
            return View("Criteria", responseModel);
        }

        
        //--------------------u-------------
        [HttpGet]
        public ActionResult Search(int isOpen = 0)
        {
            var id = WebSecurity.CurrentUserId;

            ProfileAndSearchView p = ((ProfileRepository)repository).GetProfileAndCriteriaBy(id);
            if (p.Profile == null)   return View("SearchDenial", p);
            p.isOpen = isOpen;
            return View("Search", p);
            
        }

       //-----------------------------------------

         [AllowAnonymous]
        [HttpPost]
        public ActionResult Match(SimpleMatching SimpleMatching, string ReturnUrl)
        {
          
            DateTime d1 = SimpleMatching.DOB1;
            DateTime d2 = SimpleMatching.DOB2;

            int p1 = PersonalityNumber.GetPersonalityNumberId(d1);
            int p2 = PersonalityNumber.GetPersonalityNumberId(d2);

            
            MatchResultView view = new MatchResultView();
            view.NumeralogyNumber = ((ProfileRepository)repository).GetNumeralogyInfoBy(p1, p2);

            return View("Match", view);

        }
      //----------------------------u---------------
         [AllowAnonymous]
         [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
         [AcceptVerbs(HttpVerbs.Post)]
         public JsonResult AjaxMatch(string DOB1, string DOB2, bool UseNumeralogy = false, bool UseZodiac = false)

         {
             
             int p1=0, p2=0, z1=0, z2=0;
             string sign1="", sign2="";
             NumeralogyNumber nn=null;
             NumeralogyCompatibilityLevel compN=null;
             ZodiacCompatibilityLevel compZ=null;

             DateTime d1 = Convert.ToDateTime(DOB1);
             DateTime d2 = Convert.ToDateTime(DOB2);

             if (UseNumeralogy)
             {
                

                 //Numerology
                 //---------------------------
                 p1 = PersonalityNumber.GetPersonalityNumberId(d1);
                 p2 = PersonalityNumber.GetPersonalityNumberId(d2);
                 
                 if (p1 < p2)
                      nn = ((ProfileRepository)repository).GetNumeralogyInfoBy(p1, p2);
                 else nn = ((ProfileRepository)repository).GetNumeralogyInfoBy(p2, p1);

                 compN = PersonalityNumber.GetNumeralogyCompatibility(p1, p2);
             }
             //Zodiac
             //-----------------------------
             if (UseZodiac)
             {
                 z1 = ZodiacSign.GetZodiacSignId(d1);
                 z2 = ZodiacSign.GetZodiacSignId(d2);

                 sign1 = ZodiacSign.GetZodiacSignName(z1);
                 sign2 = ZodiacSign.GetZodiacSignName(z2);

                 compZ = ZodiacSign.GetCompatibility(z1, z2);
             }

             return Json(new
             {
                 UseZodiac = UseZodiac,
                 UseNumeralogy = UseNumeralogy,

                 DOB1 = d1.ToString("MM-dd-yyyy"),
                 DOB2 = d2.ToString("MM-dd-yyyy"),

                 Number1 = UseNumeralogy? p1 : 0,
                 Number2 = UseNumeralogy? p2 :0,
                 NumeralogyInfo = UseNumeralogy? nn.Info :"",
                 NumeralogyCompatibilityLevel = UseNumeralogy? compN.Name :"",
                 NumeralogyDescription = UseNumeralogy ? compN.Description : "",

                 Sign1 = sign1,
                 Sign2 = sign2,
                 ZodiacCompatibilityLevel = UseZodiac? compZ.Name :"",
                 ZodiacDescription = UseZodiac ? compZ.Description : ""
                 
             },
               JsonRequestBehavior.AllowGet);
         }
       

      
        //-------------------u----------------------------
        [HttpPost]
        public ActionResult List()
        {
            var id = WebSecurity.CurrentUserId;
            ResponseModel pp = ((ProfileRepository)repository).FindByQuery(id, false, false);
            return View("List", pp.Profiles);
        }


        //------------------u----------------------------------
        public ActionResult SearchSearch(bool useNumerology = false, bool useZodiac = false)
        {
            var id = WebSecurity.CurrentUserId;
            ResponseModel responseModel = repository.FindByQuery(id, useNumerology, useZodiac);

            return View("List", responseModel);

        }


        //------------------u----------------------------------
        public ActionResult Details(int id)
        {

            ProfileForDisplay pp = new ProfileForDisplay();
            Profile profile = repository.FindBy(id, true);
            pp.Profile = profile;

          
            int clientId = WebSecurity.GetUserId(User.Identity.Name);
            Profile user = repository.FindBy(clientId, false);

              int p1 = profile.PersonalityNumberId;
              int p2 = user.PersonalityNumberId;
              NumeralogyNumber nn;
              
                 if (p1 < p2)
                      nn = ((ProfileRepository)repository).GetNumeralogyInfoBy(p1, p2);
                 else nn = ((ProfileRepository)repository).GetNumeralogyInfoBy(p2, p1);

                 NumeralogyCompatibilityLevel compN = PersonalityNumber.GetNumeralogyCompatibility(p1, p2);
            
             //Zodiac
             //-----------------------------
              
                int z1 = profile.ZodiacSignId;
                int z2 = user.ZodiacSignId;

                string sign1 = profile.ZodiacSignName;
                string sign2 = user.ZodiacSignName;

                ZodiacCompatibilityLevel compZ = ZodiacSign.GetCompatibility(z1, z2);

                ViewBag.NumeralogyNumberInfo = nn.Info;
                ViewBag.ZodiacCompatibilityLevel = compZ;
                ViewBag.NumeralogyCompatibilityLevel = compN;
                ViewBag.UserZodiacSignName = user.ZodiacSignName;
                ViewBag.UserPersonalityNumberId = user.PersonalityNumberId;

            return View("Details", pp);
        }
       //-------------------------------------------------------------------
       
    }
}
