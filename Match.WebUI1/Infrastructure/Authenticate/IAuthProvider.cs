﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Match.WebUI.Infrastructure.Authenticate
{
    public interface IAuthProvider
    {
        bool Authenticate(string username, string password);
    }
}